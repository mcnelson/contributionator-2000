require 'test_helper'

class GitActivitiesControllerTest < ActionDispatch::IntegrationTest
  test "retrieves latest activities" do
    VCR.use_cassette('fetch_git_activities') do
      get polymorphic_url([:git_activities]), as: :json
    end

    assert_response :success

    assert_kind_of Array, json

    # Gitlab
    assert_equal 278964, json.first.id
    assert_equal 'gitlab', json.first.serviceName
    assert_equal 'GitLab Enterprise Edition', json.first.projectLongName
    assert_equal 'gitlab-org/gitlab-ee', json.first.projectName
    assert_equal 'https://gitlab.com/gitlab-org/gitlab-ee', json.first.projectUrl
    assert_includes json.first.description, 'Enterprise Edition'
    assert_includes json.first.avatarUrl, 'assets.gitlab-static.net'
    assert_equal 42, json.first.activityCount
    assert_equal '2018-08-06T16:40:16.656Z', json.first.latestActivityAt

    # Github
    assert_equal 80035281, json.last.id
    assert_equal 'github', json.last.serviceName
    assert_equal 'SoftU2F', json.last.projectLongName
    assert_equal 'github/SoftU2F', json.last.projectName
    assert_equal 'https://github.com/github/SoftU2F', json.last.projectUrl
    assert_includes json.last.description, 'Software U2F authenticator for macOS'
    assert_nil json.last.avatarUrl
    assert_equal 1, json.last.activityCount
    assert_equal '2018-08-07T02:20:01Z', json.last.latestActivityAt
  end
end
