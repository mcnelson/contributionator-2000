require 'test_helper'

class TweetsControllerTest < ActionDispatch::IntegrationTest
  test "retrieves latest tweet" do
    VCR.use_cassette('fetch_tweet') do
      get polymorphic_url([:tweet]), as: :json
    end

    assert_response :success

    assert_includes json.text, "People are asking us if we shadow ban."
    assert_equal Time.at(1532655746).iso8601, json.createdAt
  end
end
