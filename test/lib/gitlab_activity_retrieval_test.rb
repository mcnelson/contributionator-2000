require 'test_helper'

class GitlabActivityRetrievalTest < ActiveSupport::TestCase
  test "handles 404 from Gitlab" do
    VCR.use_cassette('fetch_git_activities_gh_not_found') do
      svc = GitlabActivityRetrieval.new("gitlab-org")

      e = assert_raises(RuntimeError) do
        svc.fetch
      end

      assert_match /user.*'gitlab-org/, e.message
    end
  end
end
