require 'test_helper'

class GithubActivityRetrievalTest < ActiveSupport::TestCase
  test "handles 404 from Gitlub" do
    VCR.use_cassette('fetch_git_activities_gl_not_found') do
      svc = GithubActivityRetrieval.new("asinrroiner")

      e = assert_raises(RuntimeError) do
        svc.fetch
      end

      assert_match /user.*asinrroiner/, e.message
    end
  end
end
