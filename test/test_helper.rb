ENV['RAILS_ENV'] ||= 'test'
require_relative '../config/environment'
require 'rails/test_help'
require 'vcr'

class ActiveSupport::TestCase
  def json
    @json ||= JSON.parse(response.body, object_class: OpenStruct)
  end

  def after_teardown
    if !passed? && respond_to?(:response) && response.present?
      puts JSON.pretty_generate JSON.parse(response.body)
    end

  rescue JSON::ParserError
    nil
  end
end

VCR.configure do |config|
  config.cassette_library_dir = Rails.root.join('test', 'vcr_cassettes')
  config.hook_into :webmock
end
