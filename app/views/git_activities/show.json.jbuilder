json.array! @activities do |activity|
  json.extract! activity, :id, :description
  json.serviceName activity.integration
  json.projectName activity.name
  json.projectLongName activity.long_name
  json.projectUrl activity.url
  json.avatarUrl activity.avatar_url
  json.activityCount activity.activity_count
  json.latestActivityAt activity.latest_activity
  json.private activity.private
end
