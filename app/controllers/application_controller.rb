class ApplicationController < ActionController::API
  before_action do
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Headers'] = 'origin, content-type, accept, user-agent'
  end
end
