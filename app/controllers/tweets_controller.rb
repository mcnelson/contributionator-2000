class TweetsController < ApplicationController
  def options
    response.headers['Accept'] = 'GET'
    head :ok
  end

  def show
    r = TweetRetrieval.new Rails.application.config.env.TWITTER_HANDLE
    @tweet = r.fetch_with_cache
  end
end
