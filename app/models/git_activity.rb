class GitActivity
  include InitializedByHash
  attr_reader :id, :integration, :name, :long_name, :url, :avatar_url, :activity_count, :latest_activity,
              :private, :obscure_description

  alias :private? :private

  def description
    private? ? obscure_description : @description
  end

  def name
    private? ? '(private)' : @name
  end

  def long_name
    private? ? '(private)' : @long_name
  end

  def url
    @url unless private?
  end

  def avatar_url
    @avatar_url unless private?
  end
end
