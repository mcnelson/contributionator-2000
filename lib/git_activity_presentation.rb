class GitActivityPresentation
  attr_reader :gitlab_handle, :github_handle, :github_pat

  def initialize(gitlab_handle, github_handle, github_pat: nil)
    @gitlab_handle = gitlab_handle
    @github_handle = github_handle
    @github_pat = github_pat
  end

  def retrieve_spliced_activites
    retrieve_activities.sort_by(&:activity_count).reverse
  end

  private

  def retrieve_activities
    GitlabActivityRetrieval.new(gitlab_handle).fetch_with_cache +
      GithubActivityRetrieval.new(github_handle, github_pat).fetch_with_cache
  end
end
