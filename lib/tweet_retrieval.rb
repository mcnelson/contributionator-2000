require 'net/http'

class TweetRetrieval
  include IntegrationCommonalities
  attr_reader :handle

  def initialize(handle)
    @handle = handle
  end

  def fetch
    result = Net::HTTP.get(URI("https://twitter.com/#{handle}"))

    n = Nokogiri.parse(result)

    Tweet.new(body: n.at_css('.tweet-text').text,
              timestamp: Time.at(n.at_css('._timestamp').attr('data-time').to_i))
  end

  def fetch_with_cache
    cache_wrapper(handle) { fetch }
  end
end
