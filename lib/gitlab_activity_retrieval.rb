require 'net/http'

class GitlabActivityRetrieval
  MAX_PROJECTS = 4
  include IntegrationCommonalities
  attr_reader :handle, :json, :projects

  def initialize(handle)
    @handle = handle
  end

  def fetch
    fetch_events

    threads = project_ids.map { |id| Thread.new { fetch_project_details(id) } }
    attribute_hashes = threads.map(&:value)
    @projects = attribute_hashes.reject { |r| r.has_key?('message') }

    projects.map do |project|
      events = project_ids_into_projects.fetch(project.fetch('id'))
      newest_event = events.map { |e| e.fetch('created_at') }.sort.last

      GitActivity.new(
        project.
          slice('id', 'name', 'avatar_url', 'description').
          merge('activity_count' => events.length,
                'name' => project.fetch('path_with_namespace'),
                'long_name' => project.fetch('name'),
                'integration' => class_name_first_segment,
                'url' => project.fetch('web_url'),
                'latest_activity' => newest_event)
      )
    end
  end

  def fetch_with_cache
    cache_wrapper(handle) { fetch }
  end

  private

  def fetch_events
    uri = URI("https://gitlab.com/api/v4/users/#{handle}/events?per_page=50")
    client = Net::HTTP.new(uri.host, uri.port)
    client.use_ssl = true

    response = nil
    client.start do |h|
      req = Net::HTTP::Get.new uri
      response = h.request req

      if response.is_a?(Net::HTTPNotFound)
        raise "received 404 when fetching events, does user Gitlab '#{handle}' exist?"
      elsif !response.is_a?(Net::HTTPSuccess)
        raise "received #{response.inspect} when fetching Gitlab user's '#{handle}' events"
      end
    end

    @json = JSON.parse(response.body)
  end

  def fetch_project_details(project_id)
    result = Net::HTTP.get(URI("https://gitlab.com/api/v4/projects/#{project_id}"))
    JSON.parse(result)
  end

  def project_ids
    project_ids_into_projects.keys[0...MAX_PROJECTS]
  end

  def project_ids_into_projects
    json.group_by { |o| o.fetch('project_id') }
  end
end
